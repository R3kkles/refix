var jqFormTem;
let options = {
  beforeSubmit: showRequest,
  success: showResponse,
  dataType: 'json',
  clearForm: false,
  resetForm: false
};
var activeTag;

function refreshKaptcha() {
  $('#captcha_img').attr('src', basePath + '/sys/kaptcha?' + Math.random());
}

$(function() {
  let odiv = $('#navbar-tab');
  $('#nav-left').click(function() {
    odiv.scrollLeft(odiv.scrollLeft() - 80);
  });
  $('#nav-right').click(function() {
    odiv.scrollLeft(odiv.scrollLeft() + 80);
  });
  $('form').submit(function() {
    submitForm($(this).find('input[type="submit"]:not(.cancel), button'), null, null);
    return false;
  });
});

/**
 * 打开tag
 * @param code
 * @param name
 * @param url
 */
function openTag(code, name, url) {
  const welcome = $('#welcome');
  const li_id = '#' + code + '-li';
  const a_id = '#' + code + '-tab';
  const idDom = $(li_id);
  if (!welcome.hasClass('d-none')) {
    welcome.toggleClass('d-none');
  }
  let param = {
    'code': code,
    'name': name,
    'url': url
  };
  if (idDom.html() === undefined) {
    $.ajax({
      contentType: 'application/json;charset=utf-8',
      type: 'post',
      url: basePath + '/sys/freshenTag',
      data: JSON.stringify(param),
      success: function(data) {
        $('#navbar-tab').append($(data).children()[0]);
        $('#navbar-tab-body').append($(data).children()[1]);
        var temp = document.querySelector('#navbar-tab li:last-child a');
        temp.addEventListener('shown.bs.tab', function(event) {
          activeTag = event.target;
        });
        new bootstrap.Tab(temp).show();
      }
    });
  } else {
    new bootstrap.Tab($(a_id)).show();
  }
}

/**
 * 关闭tag按钮
 * @param id1 iframe
 * @param id2 li
 */
function closeTag(id1, id2) {
  // console.log(id1 + "+" + id2);
  var dom1 = $('#' + id1);
  var dom2 = $('#' + id2);
  dom1.remove();
  dom2.remove();
  if (dom2.children().attr('id') === $(activeTag).attr('id')) {
    if ($('#navbar-tab').children().length > 0) {
      new bootstrap.Tab($('#navbar-tab li:last-child a')).show();
    } else {
      $('#welcome').removeClass('d-none');
    }
  }
}

function refresh() {
  self.location.reload();
}

function showRequest(formData, jqForm, options) {
  jqFormTem = jqForm;
  if (formData.length === 0) {
    const json = {
      msg: '提交数据为空，请检查。'
    };
    showResponse(json, '');
    return false;
  }
  return true;
}

function showResponse(responseText, statusText) {
  resetForm(jqFormTem);
  getTipResult(jqFormTem, responseText.msg);
}

function submitForm(obj, url, formId) {
  var form = formId;
  if ($(obj)[0].hasAttribute('checkbox-check')) {
    if (formId === null) {
      form = $(obj).find('form').attr('id');
    }
    form = $('#' + form);
    if (!form.find('input[type="checkbox"]').is(':checked') && !form.find('input[type="radio"]').is(':checked')) {
      getTipResult(obj, '提交数据为空，请检查。');
      return;
    }
  }
  getTipConfirm(obj, url, formId);
}

function getTipConfirm(obj, url, formId) {
  $.ajax({
    type: 'post',
    url: basePath + '/sys/tip',
    data: {
      str: '是否继续',
      html: 'tipConfirm'
    },
    success: function(data) {
      url === null ? obj.parents('body').append(data) : $(obj).parents('body').append(data);
      const tipConfirm = $('#tipConfirm');
      const tipConfirmBoot = new bootstrap.Modal(tipConfirm, {
        backdrop: false,
        focus: true
      });
      tipConfirmBoot.show();
      tipConfirm.on('hidden.bs.modal', function() {
        this.remove();
      });
      $('#tipConfirmSubmit').click(function() {
        tipConfirm.remove();
        submit(obj, url, formId);
      });
    }
  });
}

function getTipResult(obj, str) {
  $.ajax({
    type: 'post',
    url: basePath + '/sys/tip',
    data: {
      str: str,
      html: 'tipResult'
    },
    success: function(data) {
      $(obj).parents('body').append(data);
      const tipResult = $('#tipResult');
      const tipResultBoot = new bootstrap.Modal(tipResult, {
        backdrop: false,
        keyboard: true
      });
      tipResultBoot.show();
      tipResult.on('hidden.bs.modal', function() {
        this.remove();
      });
    }
  });
}

function submit(obj, url, formId) {
  let options2 = {
    beforeSubmit: showRequest,
    success: showResponse,
    dataType: 'json',
    url: basePath + url,
    clearForm: false,
    resetForm: false
  };
  if (formId === null) {
    if (url === null) {
      $(obj).parents('form').ajaxSubmit(options);
    } else {
      $(obj).parents('form').ajaxSubmit(options2);
    }
  } else {
    if (url === null) {
      $('#' + formId).ajaxSubmit(options);
    } else {
      $('#' + formId).ajaxSubmit(options2);
    }
  }
}

function openTree(obj) {
  const btn = $(obj);
  if (btn.hasClass('bi bi-plus-circle-fill')) {
    btn.removeClass('bi bi-plus-circle-fill').addClass('bi bi-dash-circle-fill');
  } else {
    btn.removeClass('bi bi-dash-circle-fill').addClass('bi bi-plus-circle-fill');
  }
}

function treeOnclick(method, parameter) {
  const fn = window[method];
  if (fn && typeof fn === 'function') {
    eval(method + '("' + parameter + '")');
  }
}

function resetForm(id) {
  $(':input', id)
      .not(':submit, :reset, :button, :checkbox')
      .val('');
  $(':input', id)
      .filter(':checkbox, :radio')
      .removeAttr('checked')
      .removeAttr('selected');
}

function getTree(obj, name, link, checkbox, selects, linkAction) {
  $.ajax({
    type: 'post',
    url: basePath + '/sys/getTree',
    data: {
      //需要生成的树名称
      name: name,
      //树列表是否点击超链接
      link: link,
      //树是否需要选择框
      checkbox: checkbox,
      //选择框是否需要单选
      selects: selects,
      //单击方法
      linkAction: linkAction
    },
    success: function(data) {
      // console.log(data)
      $(obj).parent().append(data);
      const treeModal = $('#TreeModal');
      const treeModalBoot = new bootstrap.Modal(treeModal, {
        keyboard: true,
        focus: true
      });
      treeModalBoot.toggle();
      treeModal.on('hidden.bs.modal', function() {
        this.remove();
      });
      $('#treeModalSubmit').click(function() {
        if ($('#TreeModal').find('input').attr('type') === 'radio') {
          const temp = $('#TreeModal input:radio:checked');
          $(obj).val(temp.val());
          $(obj).next().val(temp.val());
          $(obj).next().next().val(temp.next().val());
          treeModalBoot.toggle();
        } else {
          let id = '', name = '';
          $.each($('#TreeModal input:checkbox:checked'), function() {
            name += $(this).val() + '，';
            id += $(this).next().val() + '，';
          });
          $(obj).val(name.substring(0, name.length - 1));
          $(obj).next().val(name.substring(0, name.length - 1));
          $(obj).next().next().val(id.substring(0, id.length - 1));
          treeModalBoot.toggle();
        }
      });
    }
  });
}

function getIcon(obj) {
  $.ajax({
    type: 'post',
    url: basePath + '/sys/getIcon',
    data: {},
    success: function(data) {
      $(obj).parent().append(data);
      const iconModal = $('#iconModal');
      const treeModalBoot = new bootstrap.Modal(iconModal, {
        keyboard: true,
        focus: true
      });
      treeModalBoot.toggle();
      iconModal.on('hidden.bs.modal', function(event) {
        this.remove();
      });
      $('#iconModalSubmit').click(function() {
        const temp = $('#iconModal input:radio:checked');
        $(obj).val(temp.val());
        $(obj).next().val(temp.val());
        treeModalBoot.toggle();
      });
    }
  });
}

function pageLoad(id, url, num) {
  var group = $('[search-id]');
  let param = new Map;
  for (let i = 0; i < group.length; i++) {
    let val = $(group[i]).val();
    let key = $(group[i]).attr('search-id');
    param.set(key, val);
  }
  param.set('search', 'true');
  param.set('num', num);
  let obj = Object.create(null);
  for (let [k, v] of param) {
    obj[k] = v;
  }
  $.ajax({
    type: 'post',
    url: basePath + url,
    contentType: 'application/json;charset=utf-8',
    data: JSON.stringify(obj),
    success: function(data) {
      $('#' + id).html(data);
    }
  });
}

function getSelect(obj, id) {
  if ($(obj).children().length === 0 || $(obj).children('option').attr('option-default') !== undefined) {
    $.ajax({
      type: 'post',
      url: basePath + '/sys/getDictSelect',
      data: {
        id: id
      },
      success: function(data) {
        console.log(data);
        $(obj).html(data);
      }
    });
  }
}

function openNewWindow(obj, url, formId) {
  var key, form;
  if (formId !== null && formId !== '') {
    form = $('#' + formId);
    if ($(obj)[0].hasAttribute('checkbox-check')) {
      if (!form.find('input[type="checkbox"]').is(':checked') || form.find('input[type="checkbox"]:checked').length > 1) {
        getTipResult(obj, '请选择一条数据。');
        return;
      }
      key = $('[primary-key]:checked').attr('primary-key');
    } else {
      key = $('[primary-key]').attr('primary-key');
    }
  }
  $.ajax({
    type: 'post',
    url: basePath + '/sys/openNewWindow',
    data: {
      url: url,
      key: key
    },
    success: function(data) {
      $(obj).parents('body').append(data);
      var windowModal = $('#' + $(data).attr('id'));
      const windowModalBoot = new bootstrap.Modal(windowModal, {
        backdrop: false,
        keyboard: true
      });
      windowModalBoot.show();
      windowModal.on('hidden.bs.modal', function() {
        this.remove();
      });
    }
  });
}