/*
 Navicat Premium Data Transfer

 Source Server         : oudihong
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : localhost:3306
 Source Schema         : refix

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 13/07/2021 14:10:04
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for persistent_logins
-- ----------------------------
DROP TABLE IF EXISTS `persistent_logins`;
CREATE TABLE `persistent_logins`  (
  `username` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `series` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `last_used` timestamp(0) NOT NULL,
  PRIMARY KEY (`series`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of persistent_logins
-- ----------------------------

-- ----------------------------
-- Table structure for sys_area
-- ----------------------------
DROP TABLE IF EXISTS `sys_area`;
CREATE TABLE `sys_area`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编码',
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '区域名称',
  `seq` int(11) NULL DEFAULT NULL COMMENT '排序',
  `parent_id` int(11) NULL DEFAULT 0 COMMENT '父菜单id',
  `parent_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父菜单Name',
  `flag` tinyint(2) NULL DEFAULT NULL COMMENT '标志，用于判断是否最后子菜单（1为是、0为否）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '区域管理表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_area
-- ----------------------------
INSERT INTO `sys_area` VALUES (1, '440100', '示例市1', 2, 0, '', 0);
INSERT INTO `sys_area` VALUES (2, '440101', '示例区1', 1, 1, '示例市1', 0);
INSERT INTO `sys_area` VALUES (3, '440102', '示例区2', 2, 1, '示例市1', 0);
INSERT INTO `sys_area` VALUES (4, '440103', '示例街1', 1, 2, '示例区1', 1);
INSERT INTO `sys_area` VALUES (5, '440104', '示例街1', 2, 2, '示例区1', 1);
INSERT INTO `sys_area` VALUES (6, '440105', '示例街2', 1, 3, '示例区2', 1);
INSERT INTO `sys_area` VALUES (7, '440106', '示例街2', 2, 3, '示例区2', 1);
INSERT INTO `sys_area` VALUES (8, '440107', '示例市2', 3, 0, '', 0);
INSERT INTO `sys_area` VALUES (9, '440108', '示例区1', 1, 8, '示例市2', 0);
INSERT INTO `sys_area` VALUES (10, '440109', '示例区2', 2, 8, '示例市2', 0);
INSERT INTO `sys_area` VALUES (11, '440110', '示例街1', 1, 9, '示例区1', 1);
INSERT INTO `sys_area` VALUES (12, '440111', '示例街1', 2, 9, '示例区1', 1);
INSERT INTO `sys_area` VALUES (13, '440112', '示例街2', 1, 10, '示例区2', 1);
INSERT INTO `sys_area` VALUES (14, '440113', '示例街2', 2, 10, '示例区2', 0);
INSERT INTO `sys_area` VALUES (15, '440114', '示例街3', 1, 14, '示例街2', 1);
INSERT INTO `sys_area` VALUES (16, '440115', '特殊经济区', 1, 0, '', 1);

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典编码',
  `code_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `value` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_code`(`code`) USING BTREE,
  INDEX `idx_value`(`value`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES (1, '禁用', 'isUsed', '启用状态', '0', '启用状态');
INSERT INTO `sys_dict` VALUES (2, '启用', 'isUsed', '启用状态', '1', '启用状态');
INSERT INTO `sys_dict` VALUES (22, '管理员', 'role', '权限角色', '1', 'value是权限id');
INSERT INTO `sys_dict` VALUES (23, '用户', 'role', '权限角色', '2', 'value是权限id');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编码',
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `seq` int(11) NULL DEFAULT NULL COMMENT '排序',
  `icon` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '图标',
  `parent_id` int(11) NULL DEFAULT 0 COMMENT '父菜单id',
  `parent_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父菜单Name',
  `url` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '地址',
  `flag` tinyint(2) NULL DEFAULT NULL COMMENT '菜单标志，用于判断是否最后子菜单（1为是、0为否）',
  `display` tinyint(2) NULL DEFAULT NULL COMMENT '菜单是否可见（1为可见，2为不可见',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 49 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, 'sys', '系统', 2, '', 0, '', '', 0, 1);
INSERT INTO `sys_menu` VALUES (2, 'sys_man', '系统管理', 2, 'bi bi-gear-fill', 1, '系统', '', 0, 1);
INSERT INTO `sys_menu` VALUES (3, 'base_man', '基础管理', 1, '', 2, '系统管理', '', 0, 1);
INSERT INTO `sys_menu` VALUES (4, 'menu_man', '菜单管理', 1, '', 3, '基础管理', '/sys/toMenuMan', 1, 1);
INSERT INTO `sys_menu` VALUES (5, 'user_man', '账号管理', 2, '', 3, '基础管理', '/sys/toUserMan', 1, 1);
INSERT INTO `sys_menu` VALUES (6, 'role_man', '权限管理', 3, '', 3, '基础管理', '/sys/toRoleMan', 1, 1);
INSERT INTO `sys_menu` VALUES (7, 'area_man', '地区管理', 4, '', 3, '基础管理', '/sys/toAreaMan', 1, 1);
INSERT INTO `sys_menu` VALUES (8, 'org_man', '组织管理', 5, '', 3, '基础管理', '/sys/toOrgMan', 1, 1);
INSERT INTO `sys_menu` VALUES (9, 'dict_man', '字典管理', 5, '', 3, '基础管理', '/sys/toDictMan', 1, 1);
INSERT INTO `sys_menu` VALUES (21, 'workbench', '工作台', 1, 'bi bi-house-fill', 0, '', '/sys/workbench', 1, 1);
INSERT INTO `sys_menu` VALUES (47, 'test02', '业务', 3, '', 0, '', '', 0, 1);
INSERT INTO `sys_menu` VALUES (48, 'test02', '测试（业务）', 1, 'bi bi-google', 47, '业务', '', 1, 1);

-- ----------------------------
-- Table structure for sys_menu_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu_role`;
CREATE TABLE `sys_menu_role`  (
  `m_id` int(20) NOT NULL COMMENT '菜单id',
  `r_id` int(20) NOT NULL COMMENT '权限id'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_menu_role
-- ----------------------------
INSERT INTO `sys_menu_role` VALUES (4, 1);
INSERT INTO `sys_menu_role` VALUES (44, 2);
INSERT INTO `sys_menu_role` VALUES (46, 2);
INSERT INTO `sys_menu_role` VALUES (47, 1);
INSERT INTO `sys_menu_role` VALUES (47, 2);
INSERT INTO `sys_menu_role` VALUES (48, 1);
INSERT INTO `sys_menu_role` VALUES (48, 2);
INSERT INTO `sys_menu_role` VALUES (2, 1);
INSERT INTO `sys_menu_role` VALUES (3, 1);
INSERT INTO `sys_menu_role` VALUES (5, 1);
INSERT INTO `sys_menu_role` VALUES (7, 1);
INSERT INTO `sys_menu_role` VALUES (8, 1);
INSERT INTO `sys_menu_role` VALUES (9, 1);
INSERT INTO `sys_menu_role` VALUES (6, 1);
INSERT INTO `sys_menu_role` VALUES (1, 1);
INSERT INTO `sys_menu_role` VALUES (21, 1);
INSERT INTO `sys_menu_role` VALUES (21, 2);

-- ----------------------------
-- Table structure for sys_org
-- ----------------------------
DROP TABLE IF EXISTS `sys_org`;
CREATE TABLE `sys_org`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编码',
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组织名称',
  `seq` int(11) NULL DEFAULT NULL COMMENT '排序',
  `parent_id` int(11) NULL DEFAULT 0 COMMENT '父菜单id',
  `parent_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父菜单Name',
  `area_id` int(11) NULL DEFAULT 0 COMMENT '地区id',
  `flag` tinyint(2) NULL DEFAULT NULL COMMENT '标志，用于判断是否最后子菜单（1为是、0为否）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_org
-- ----------------------------
INSERT INTO `sys_org` VALUES (1, '10000', '示例省级单位', 1, 0, '', 1, 0);
INSERT INTO `sys_org` VALUES (2, '10001', '示例市级单位', 1, 1, '示例省级单位', 2, 0);
INSERT INTO `sys_org` VALUES (3, '10002', '示例区级单位', 1, 2, '示例市级单位', 3, 0);
INSERT INTO `sys_org` VALUES (4, '10003', '示例街道办', 1, 3, '示例区级单位', 4, 1);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `role_code` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色名称',
  `role_name` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色描述',
  `remark` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, 'ROLE_Admin', '管理员', '管理员,拥有最高权力，能够修改系统基础设置，请谨慎将该权限角色赋予账号。');
INSERT INTO `sys_role` VALUES (2, 'ROLE_User', '用户', '普通用户');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `username` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名称',
  `password` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `status` int(2) NULL DEFAULT NULL COMMENT '1开启0关闭',
  `id_cart` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '身份证',
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电话',
  `area_id` int(11) NULL DEFAULT NULL COMMENT '地区id',
  `org_id` int(11) NULL DEFAULT NULL COMMENT '部门id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, '管理员', 'admin', '$2a$10$u8AT2q1BnVGrfFZMbGvHLe6R7icDGIPcu9r1aTuN10BCsNBFrum1O', 1, '222222222222222222', '11111111111', 1, 1);
INSERT INTO `sys_user` VALUES (2, '用户', '123', '$2a$10$HdAR0BKX0VL0u6xkyRmzFOKcA6vNJIUaZrrZBY3N4kTnSzDfFLWey', 1, '123123123', '12312313123', 1, 1);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `uid` int(11) NOT NULL COMMENT '用户编号',
  `rid` int(11) NOT NULL COMMENT '角色编号',
  PRIMARY KEY (`uid`, `rid`) USING BTREE,
  INDEX `FK_Reference_10`(`rid`) USING BTREE,
  CONSTRAINT `FK_Reference_10` FOREIGN KEY (`rid`) REFERENCES `sys_role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_Reference_9` FOREIGN KEY (`uid`) REFERENCES `sys_user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 2);

SET FOREIGN_KEY_CHECKS = 1;
