package com.refix.refix.entity;

import com.refix.refix.Interface.DataName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * @Author ODH
 * @Date 2021/2/15 下午 03:26
 * @description
 * @Version 1.0
 */
@Getter
@Setter
@ToString
public class SysMenu extends base implements Serializable {
  @DataName(name = "编码")
  private String code;
  @DataName(name = "是否可见")
  private boolean display;
  @DataName(name = "树标志")
  private boolean flag;
  @DataName(name = "图标")
  private String icon;
  @DataName(name = "ID")
  private Integer id;
  @DataName(name = "名称")
  private String name;
  @DataName(name = "上级ID")
  private Integer parentId = 0;
  @DataName(name = "父名称")
  private String parentName;
  @DataName(name = "权限列表")
  private List<SysRole> rid;
  @DataName(name = "排序")
  private Integer seq;
  @DataName(name = "链接地址")
  private String url;
  private List<SysMenu> childList;

}
