package com.refix.refix.entity;

import com.refix.refix.enums.JsonReturnCode;

/**
 * @Author ODH
 * @Date 2021/5/18 下午 01:42
 * @description
 * @Version 1.0
 */
public class JsonResult<T> {
    private String code;
    private String msg;
    private T data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }


    @Override
    public String toString() {
        return "code=" + code + " message=" + msg + " data=" + data;
    }

    public static <T> JsonResult<T> fail() {
        JsonResult<T> ret = new JsonResult<T>();
        ret.setCode(JsonReturnCode.FAIL.getCode());
        ret.setMsg(JsonReturnCode.FAIL.getDesc());
        return ret;
    }

    public static <T> JsonResult<T> failException() {
        JsonResult<T> ret = new JsonResult<T>();
        ret.setCode(JsonReturnCode.FAIL_EXCEPTION.getCode());
        ret.setMsg(JsonReturnCode.FAIL_EXCEPTION.getDesc());
        return ret;
    }

    public static <T> JsonResult<T> failSession() {
        JsonResult<T> ret = new JsonResult<T>();
        ret.setCode(JsonReturnCode.FAIL_SESSION.getCode());
        ret.setMsg(JsonReturnCode.FAIL_SESSION.getDesc());
        return ret;
    }

    public static <T> JsonResult<T> failException(T data) {
        JsonResult<T> ret = JsonResult.failException();
        ret.setData(data);
        return ret;
    }

    public static <T> JsonResult<T> fail(T data) {
        JsonResult<T> ret = JsonResult.fail();
        ret.setData(data);
        return ret;
    }

    public static <T> JsonResult<T> failMessage(String msg) {
        JsonResult<T> ret = JsonResult.fail();
        ret.setMsg(msg);
        return ret;
    }

    public static <T> JsonResult<T> successMessage(String msg) {
        JsonResult<T> ret = JsonResult.success();
        ret.setMsg(msg);
        return ret;
    }

    public static <T> JsonResult<T> success() {
        JsonResult<T> ret = new JsonResult<T>();
        ret.setCode(JsonReturnCode.SUCCESS.getCode());
        ret.setMsg(JsonReturnCode.SUCCESS.getDesc());
        return ret;
    }


    public static <T> JsonResult<T> success(T data) {
        JsonResult<T> ret = JsonResult.success();
        ret.setData(data);
        return ret;
    }

    public static <T> JsonResult<T> http404(T data) {
        JsonResult<T> ret = new JsonResult<T>();
        ret.setCode(JsonReturnCode.NOT_FOUND.getCode());
        ret.setMsg(JsonReturnCode.NOT_FOUND.getDesc());
        ret.setData(data);
        return ret;
    }

    public static <T> JsonResult<T> http403(T data) {
        JsonResult<T> ret = new JsonResult<T>();
        ret.setCode(JsonReturnCode.ACCESS_ERROR.getCode());
        ret.setMsg(JsonReturnCode.ACCESS_ERROR.getDesc());
        ret.setData(data);
        return ret;
    }

    public static <T> JsonResult<T> http401(T data) {
        JsonResult<T> ret = new JsonResult<T>();
        ret.setCode(JsonReturnCode.NOT_LOGIN.getCode());
        ret.setMsg(JsonReturnCode.NOT_LOGIN.getDesc());
        ret.setData(data);
        return ret;
    }

    public static <T> JsonResult<T> same(T data) {
        JsonResult<T> ret = JsonResult.status2000(data);
        ret.setData(data);
        return ret;
    }

    public static <T> JsonResult<T> status2000(T data) {
        JsonResult<T> ret = new JsonResult<T>();
        ret.setCode(JsonReturnCode.SAME.getCode());
        ret.setMsg(JsonReturnCode.SAME.getDesc());
        ret.setData(data);
        return ret;
    }

    public static <T> JsonResult<T> successMsg(T data) {
        JsonResult<T> ret = JsonResult.successMessage(data);
        ret.setData(data);
        return ret;
    }

    public static <T> JsonResult<T> successMessage(T data) {
        JsonResult<T> ret = new JsonResult<T>();
        ret.setCode(JsonReturnCode.SUCCESSMSG.getCode());
        ret.setMsg(JsonReturnCode.SUCCESSMSG.getDesc());
        ret.setData(data);
        return ret;
    }
}