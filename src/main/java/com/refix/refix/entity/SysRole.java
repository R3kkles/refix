package com.refix.refix.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.refix.refix.Interface.DataName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;

/**
 * @Author ODH
 * @Date 2021/2/8 上午 10:05
 * @description
 * @Version 1.0
 */
@Setter
@Getter
@ToString
public class SysRole extends base implements GrantedAuthority, Serializable {
    @DataName(name = "ID")
    private Integer id;
    @DataName(name = "编码")
    private String roleCode;
    @DataName(name = "名称")
    private String roleName;
    @DataName(name = "备注")
    private String remark;
    private boolean checked;

    @JsonIgnore
    @Override
    public String getAuthority() {
        return roleCode;
    }
}
