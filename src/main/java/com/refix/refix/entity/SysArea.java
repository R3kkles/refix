package com.refix.refix.entity;

import com.refix.refix.Interface.DataName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * @Author ODH
 * @Date 2021/7/8 上午 10:29
 * @Describe
 * @Version 1.0
 */
@Getter
@Setter
@ToString
public class SysArea extends base implements Serializable {
  @DataName(name = "ID")
  private Integer id;
  @DataName(name = "名称")
  private String name;
  @DataName(name = "上级ID")
  private Integer parentId =0;
  @DataName(name = "父名称")
  private String parentName;
  @DataName(name = "编码")
  private String code;
  @DataName(name = "标志，用于判断是否最后子菜单")
  private boolean flag;
  @DataName(name = "排序")
  private Integer seq;
  private List<SysArea> childList;
}
