package com.refix.refix.entity;

import com.refix.refix.Interface.DataName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author ODH
 * @Date 2021/7/4 上午 11:01
 * @Describe 树实体类
 * @Version 1.0
 */
@Getter
@Setter
@AllArgsConstructor
public class SysTree extends base{
  /*
    是否需要选择按钮
   */
  @DataName(name = "是否需要选择按钮")
  private boolean checkbox;
  /*
    是否需要单击触发
   */
  @DataName(name = "是否需要单击触发")
  private boolean link;
  /*
  是否单选
   */
  @DataName(name = "单击触发方法")
  private String linkAction = "";
  /*
  是否单选
   */
  @DataName(name = "是否单选")
  private boolean selects;

  public SysTree(boolean checkbox, boolean link) {
    this.checkbox = checkbox;
    this.link = link;
  }

  public SysTree(boolean checkbox, boolean link, String linkAction) {
    this.checkbox = checkbox;
    this.link = link;
    this.linkAction = linkAction;
  }
}
