package com.refix.refix.entity;

import com.refix.refix.Interface.DataName;
import lombok.*;

import java.io.Serializable;

/**
 * @Author ODH
 * @Date 2021/3/10 上午 09:52
 * @description
 * @Version 1.0
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SysDict extends base implements Serializable {
    @DataName(name = "id")
    private String id;
    @DataName(name = "值")
    private String value;
    @DataName(name = "名称")
    private String name;
    @DataName(name = "字典编码")
    private String code;
    @DataName(name = "字典类别")
    private String codeName;
    @DataName(name = "备注")
    private String remark;
}
