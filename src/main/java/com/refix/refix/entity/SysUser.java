package com.refix.refix.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.refix.refix.Interface.DataName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

/**
 * @Author ODH
 * @Date 2021/2/8 上午 10:04
 * @description
 * @Version 1.0
 */
@Setter
@Getter
@ToString
public class SysUser extends base implements UserDetails {
  @DataName(name = "id")
  private Integer id;
  @DataName(name = "账号")
  private String username;
  private String password;
  @DataName(name = "状态")
  private String status;
  private String statusName;
  @DataName(name = "电话")
  private String phone;
  @DataName(name = "身份证")
  private String idCart;
  @DataName(name = "地区")
  private Integer areaId;
  private String areaName;
  @DataName(name = "组织")
  private Integer orgId;
  private String orgName;
  @DataName(name = "姓名")
  private String name;
  @DataName(name = "权限")
  private String role;
  private String roleName;
  private List<SysRole> roles;

  @JsonIgnore
  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return roles;
  }

  @Override
  public String getPassword() {
    return password;
  }

  @Override
  public String getUsername() {
    return username;
  }

  @JsonIgnore
  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @JsonIgnore
  @Override
  public boolean isAccountNonLocked() {
    return true;
  }

  @JsonIgnore
  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @JsonIgnore
  @Override
  public boolean isEnabled() {
    return true;
  }

}
