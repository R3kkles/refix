package com.refix.refix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.scheduling.annotation.EnableAsync;

@Configuration
@SpringBootApplication
@EnableCaching
@EnableAsync
@ServletComponentScan
@ImportResource(locations = {"classpath:kaptcha/kaptcha.xml"})
public class RefixApplication {
  public static void main(String[] args) {
    SpringApplication.run(RefixApplication.class, args);
  }
}
