package com.refix.refix.service;

import com.refix.refix.entity.*;

import java.util.List;
import java.util.Map;

/**
 * @Author ODH
 * @Date 2021/3/1 上午 10:04
 * @description
 * @Version 1.0
 */
public interface SysService {

  int deleteArea(Integer[] id);

  int deleteOrg(Integer[] id);

  int deleteUser(Integer[] id);

  int insertUser(SysUser sysUser);

  /**
   * 获取菜单树
   *
   * @return
   */
  List<SysMenu> getMenuTree();

  /**
   * 获取地区树
   *
   * @return
   */
  List<SysArea> getAreaTree();

  /**
   * 获取组织树
   *
   * @return
   */
  List<SysOrg> getOrgTree();

  /**
   * 查询单个字典
   *
   * @param id
   * @return
   */
  SysDict findDictById(String id);

  /**
   * 所有字典
   *
   * @return
   */
  List<SysDict> queryAllDict(Map<String, Object> param);

  /**
   * 查询日志
   *
   * @return
   */
  List<SysLog> queryAllLog(Map<String, String> param);

  /**
   * 权限角色树
   *
   * @return
   */
  List<SysRole> queryAllRoles(Map<String, Object> param);

  /**
   * 查询字典
   *
   * @param code
   * @return
   */
  List<SysDict> queryDict(List<String> code);

  SysArea queryOneArea(String id);

  SysDict queryOneDict(Integer id);

  /**
   * 查找单个菜单
   *
   * @param id
   * @return
   */
  SysMenu queryOneMenu(String id);

  /**
   * 查询一个role
   *
   * @param id
   * @return
   */
  List<SysRole> queryCheckRole(Integer id);

  SysOrg queryOneOrg(String id);

  /**
   * 查找一个权限角色
   *
   * @param key
   * @return
   */
  SysRole queryOneRole(Integer key);

  /**
   * 根据id查询用户对应权限
   *
   * @param id
   * @return
   */
  Map<String, Integer> queryUserRoles(String id);

  /**
   * 查找所有用户（包含搜索）
   *
   * @return
   */
  List<SysUser> queryAllUser(Map<String, Object> param);

  /**
   * 查询单个user
   *
   * @param id
   * @return
   */
  SysUser queryOneUser(Integer id);

  /**
   * 获取树
   *
   * @param name
   * @return
   */
  Object getTree(String name);

  /**
   * 新增字典
   *
   * @param sysDict
   * @return
   */
  int insertDict(SysDict sysDict);

  /**
   * 写入日志
   *
   * @param sysLog
   * @return
   */
  int insertLog(SysLog sysLog);

  /**
   * 新增权限
   *
   * @param sysRole
   * @return
   */
  int insertRole(SysRole sysRole);

  int updateArea(SysArea sysArea);

  int updateMenu(SysMenu sysMenu);

  int updateOrg(SysOrg sysOrg);

  int updateUser(SysUser sysUser);

  /**
   * 更新字典
   *
   * @param sysDict
   * @return
   */
  int updateDict(SysDict sysDict);

  /**
   * 更新role
   *
   * @param sysRole
   * @return
   */
  int updateRole(SysRole sysRole);

  /**
   * 删除字典
   *
   * @param id
   * @return
   */
  int deleteDict(Integer[] id);

  int deleteMenu(Integer[] id);

  /**
   * 删除权限
   *
   * @param id
   * @return
   */
  int deleteRole(Integer[] id);
}
