package com.refix.refix.service.Impl;

import com.refix.refix.dao.SysDao;
import com.refix.refix.entity.*;
import com.refix.refix.service.SysService;
import com.refix.refix.util.DictUtil;
import com.refix.refix.util.SessionUtil;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author ODH
 * @Date 2021/2/15 下午 04:05
 * @description
 * @Version 1.0
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysServiceImpl implements SysService {
  @Resource
  private SysDao sysDao;
  @Resource
  private BCryptPasswordEncoder passwordEncoder;


  @Override
  public int deleteDict(Integer[] id) {
    return sysDao.deleteDict(id);
  }

  @Override
  public int deleteMenu(Integer[] id) {
    sysDao.deleteMenuRole(id);
    return sysDao.deleteMenu(id);
  }

  @Override
  public int deleteRole(Integer[] id) {
    sysDao.deleteUserRoleByRID(id);
    return sysDao.deleteRole(id);
  }

  @Override
  public int deleteArea(Integer[] id) {
    return sysDao.deleteArea(id);
  }

  @Override
  public int deleteOrg(Integer[] id) {
    return sysDao.deleteOrg(id);
  }

  @Override
  public int deleteUser(Integer[] id) {
    sysDao.deleteUserRoleByUID(id);
    return sysDao.deleteUser(id);
  }

  @Override
  public int insertUser(SysUser sysUser) {
    sysUser.setPassword(passwordEncoder.encode(sysUser.getPassword()));
    if (sysDao.insertUser(sysUser) > 0) {
      return sysDao.insertUserRole(sysUser);
    }
    return 0;
  }

  @Override
  public List<SysMenu> getMenuTree() {
    SysUser sysUser = (SysUser) SessionUtil.getSession().getAttribute("user");
    Integer role = sysUser.getRoles().get(0).getId();
    return sysDao.getMenuTree(role);
  }

  @Override
  public List<SysArea> getAreaTree() {
    return sysDao.getAreaTree();
  }

  @Override
  public List<SysOrg> getOrgTree() {
    return sysDao.getOrgTree();
  }

  @Override
  public SysDict findDictById(String id) {
    return sysDao.findDictById(id);
  }

  @Override
  public Object getTree(String name) {
    String method = "get" + name + "Tree";
    Object list = null;
    try {
      list = this.getClass().getMethod(method, new Class[]{}).invoke(this);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return list;
  }

  @Override
  public int insertDict(SysDict sysDict) {
    return sysDao.insertDict(sysDict);
  }

  @Override
  public int insertLog(SysLog sysLog) {
    return sysDao.insertLog(sysLog);
  }

  @Override
  public int insertRole(SysRole sysRole) {
    return sysDao.insertRole(sysRole);
  }

  @Override
  public int updateArea(SysArea sysArea) {
    if (sysArea.getId() != null) {
      return sysDao.updateArea(sysArea);
    } else {
      return sysDao.insertArea(sysArea);
    }
  }

  @Override
  public List<SysDict> queryAllDict(Map<String, Object> param) {
    return sysDao.queryAllDict(param);
  }

  @Override
  public List<SysLog> queryAllLog(Map<String, String> param) {
    return sysDao.queryAllLog(param);
  }

  @Override
  public List<SysRole> queryAllRoles(Map<String, Object> param) {
    return sysDao.queryAllRoles(param);
  }

  @Override
  public List<SysDict> queryDict(List<String> code) {
    return sysDao.queryDict(code);
  }

  @Override
  public SysArea queryOneArea(String id) {
    return sysDao.queryOneArea(id);
  }

  @Override
  public SysDict queryOneDict(Integer id) {
    return sysDao.queryOneDict(id);
  }

  @Override
  public SysMenu queryOneMenu(String id) {
    return sysDao.queryOneMenu(id);
  }

  @Override
  public List<SysRole> queryCheckRole(Integer id) {
    List<SysRole> temp = sysDao.queryCheckRole(id);
    List<SysRole> full = sysDao.queryAllRoles(null);
    for (SysRole sysRole : full) {
      for (SysRole sysRole1 : temp) {
        if (sysRole1.getId().equals(sysRole.getId())) {
          sysRole.setChecked(true);
        }
      }
    }
    return full;
  }

  @Override
  public SysOrg queryOneOrg(String id) {
    return sysDao.queryOneOrg(id);
  }

  @Override
  public SysRole queryOneRole(Integer id) {
    return sysDao.queryOneRole(id);
  }

  @Override
  public Map<String, Integer> queryUserRoles(String id) {
    return sysDao.queryUserRoles(id);
  }

  @Override
  public int updateMenu(SysMenu sysMenu) {
    if (sysMenu.getId() != null) {
      //删除旧的映射关系
      sysDao.deleteMenuRole(new Integer[]{sysMenu.getId()});
      //插入新的映射关系
      sysDao.insertMenuRole(sysMenu.getId(), sysMenu.getRid());
      //更新菜单表
      return sysDao.updateMenu(sysMenu);
    } else {
      sysDao.insertMenu(sysMenu);
      return sysDao.insertMenuRole(sysMenu.getId(), sysMenu.getRid());
    }
  }

  @Override
  public int updateOrg(SysOrg sysOrg) {
    if (sysOrg.getId() != null) {
      return sysDao.updateOrg(sysOrg);
    } else {
      return sysDao.insertOrg(sysOrg);
    }
  }

  @Override
  public int updateUser(SysUser sysUser) {
    sysUser.setPassword(passwordEncoder.encode(sysUser.getPassword()));
    return sysDao.updateUser(sysUser);
  }

  @Override
  public int updateDict(SysDict sysDict) {
    if (sysDict.getId() != null) {
      return sysDao.updateDict(sysDict);
    } else {
      return sysDao.insertDict(sysDict);
    }
  }

  @Override
  public int updateRole(SysRole sysRole) {
    return sysDao.updateRole(sysRole);
  }

  @Override
  public List<SysUser> queryAllUser(Map<String, Object> param) {
    List<SysUser> list = sysDao.queryAllUser(param);
    Map<String, String> map = new HashMap<>();
    map.put("isUsed", "status");
    DictUtil.transformDict(sysDao, list, map);
    return list;
  }

  @Override
  public SysUser queryOneUser(Integer id) {
    List<SysUser> list = new ArrayList<>();
    list.add(sysDao.queryOneUser(id));
    Map<String, String> map = new HashMap<>();
    map.put("isUsed", "status");
    map.put("role", "role");
    DictUtil.transformDict(sysDao, list, map);
    return list.get(0);
  }
}
