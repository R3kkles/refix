package com.refix.refix.service.Impl;

import com.refix.refix.dao.UserDao;
import com.refix.refix.entity.SysRole;
import com.refix.refix.entity.SysUser;
import com.refix.refix.service.UserService;
import com.refix.refix.util.SessionUtil;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author ODH
 * @Date 2021/2/8 上午 10:00
 * @description UserServiceImpl
 * @Version 1.0
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class UserServiceImpl implements UserService {
  @Resource
  private BCryptPasswordEncoder passwordEncoder;
  @Resource
  private UserDao userDao;

  @Override
  public List<SysRole> findByUid(Integer uid) {
    return userDao.findByUid(uid);
  }

  @Override
  public SysUser findByName(String username) {
    return userDao.findByName(username);
  }

  @Override
  public int deletePeople(String username) {
    return userDao.deletePeople(username);
  }

  @Override
  public int register(SysUser sysUser) {
    return userDao.register(sysUser);
  }

  @Override
  public void insertUserRoles(Integer id) {
    userDao.insertUserRoles(id);
  }

  @Override
  public UserDetails loadUserByUsername(String username) {
    UserDetails userDetails = null;
    try {
      //根据用户名做查询
      SysUser sysUser = userDao.findByName(username);
      if (sysUser == null) {
        throw new UsernameNotFoundException("UserDetailsService returned null, which is an interface contract violation");
      }
      sysUser.setRoles(userDao.findByUid(sysUser.getId()));
      List<SimpleGrantedAuthority> authorities = new ArrayList<>();
      List<SysRole> roles = sysUser.getRoles();
      for (SysRole role : roles) {
        authorities.add(new SimpleGrantedAuthority(role.getRoleCode()));
      }
      //账户已被禁用
      if ("0".equals(sysUser.getStatus())) {
        userDetails = new User(sysUser.getUsername(), sysUser.getPassword(), false, true, true, true, authorities);
        throw new DisabledException("username is disabled");
      }
      SessionUtil.getSession().setAttribute("user", sysUser);
      userDetails = new User(sysUser.getUsername(), sysUser.getPassword(), authorities);
      return userDetails;
    } catch (Exception e) {
      //认证失败！
      return userDetails;
    }
  }


}
