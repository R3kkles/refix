package com.refix.refix.service;



import com.refix.refix.entity.SysRole;
import com.refix.refix.entity.SysUser;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

/**
 * @Author ODH
 * @Date 2021/3/1 上午 10:03
 * @description
 * @Version 1.0
 */
public interface UserService extends UserDetailsService {
    /**
     * 查找通过id
     * @param uid
     * @return
     */
    List<SysRole> findByUid(Integer uid);

    /**
     * 根据name查找
     * @param username
     * @return
     */
    SysUser findByName(String username);

    /**
     * 删除用户
     * @param username
     * @return
     */
    int deletePeople(String username);

    int register(SysUser sysUser);

    void insertUserRoles(Integer id);
}
