package com.refix.refix.config;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.refix.refix.enums.ExpireTimeType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.time.Duration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @Author ODH
 * @Date 2021/5/18 下午 01:36
 * @description
 * @Version 1.0
 */
public class RedisConfig extends CachingConfigurerSupport {

    @Value("${spring.redis.expiration:1}")
    private Integer expiration;
    @Value("refix")
    private String cachePrefixName;

    public RedisConfig() {
    }

    @Bean
    public RedisSerializer redisStringSerializer() {
        return new StringRedisSerializer();
    }

    @Bean
    public CacheManager cacheManager(RedisConnectionFactory factory) {
        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        jackson2JsonRedisSerializer.setObjectMapper(om);
        RedisCacheConfiguration rcc = RedisCacheConfiguration.defaultCacheConfig().entryTtl(Duration.ofHours((long) this.expiration)).disableCachingNullValues().computePrefixWith((cacheName) -> {
            return this.cachePrefixName.concat(":").concat(cacheName).concat(":");
        }).serializeKeysWith(RedisSerializationContext.SerializationPair.fromSerializer(new StringRedisSerializer())).serializeValuesWith(RedisSerializationContext.SerializationPair.fromSerializer(jackson2JsonRedisSerializer));
        Set<String> cacheNames = new HashSet();
        cacheNames.add("GD");
        cacheNames.add("HM");
        cacheNames.add("OM");
        cacheNames.add("FM");
        cacheNames.add("TM");
        cacheNames.add("HH");
        cacheNames.add("OH");
        cacheNames.add("TH");
        cacheNames.add("SH");
        cacheNames.add("TWH");
        cacheNames.add("OD");
        Map<String, RedisCacheConfiguration> configMap = new HashMap();
        configMap.put("GD", rcc.entryTtl(ExpireTimeType.GENERAL.toDuration()));
        configMap.put("HM", rcc.entryTtl(ExpireTimeType.HALF_MINUTE.toDuration()));
        configMap.put("OM", rcc.entryTtl(ExpireTimeType.ONE_MINUTE.toDuration()));
        configMap.put("FM", rcc.entryTtl(ExpireTimeType.FIVE_MINUTE.toDuration()));
        configMap.put("TM", rcc.entryTtl(ExpireTimeType.TEN_MINUTE.toDuration()));
        configMap.put("HH", rcc.entryTtl(ExpireTimeType.HALF_HOUR.toDuration()));
        configMap.put("OH", rcc.entryTtl(ExpireTimeType.ONE_HOUR.toDuration()));
        configMap.put("TH", rcc.entryTtl(ExpireTimeType.TWO_HOUR.toDuration()));
        configMap.put("SH", rcc.entryTtl(ExpireTimeType.SIX_HOUR.toDuration()));
        configMap.put("TWH", rcc.entryTtl(ExpireTimeType.TWELVE_HOUR.toDuration()));
        configMap.put("OD", rcc.entryTtl(ExpireTimeType.ONE_DAY.toDuration()));
        RedisCacheManager redisCacheManager = RedisCacheManager.builder(factory).initialCacheNames(cacheNames).withInitialCacheConfigurations(configMap).build();
        return redisCacheManager;
    }

    // 以下两种redisTemplate自由根据场景选择
    @Bean
    public RedisTemplate<Object, Object> redisTemplate(RedisConnectionFactory connectionFactory) {
        RedisTemplate<Object, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(connectionFactory);

        //使用Jackson2JsonRedisSerializer来序列化和反序列化redis的value值（默认使用JDK的序列化方式）
        Jackson2JsonRedisSerializer serializer = new Jackson2JsonRedisSerializer(Object.class);

        ObjectMapper mapper = new ObjectMapper();
        mapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        serializer.setObjectMapper(mapper);

        template.setValueSerializer(serializer);
        //使用StringRedisSerializer来序列化和反序列化redis的key值
        template.setKeySerializer(new StringRedisSerializer());
        template.afterPropertiesSet();
        return template;
    }

    @Bean
    public StringRedisTemplate stringRedisTemplate(RedisConnectionFactory factory) {
        StringRedisTemplate stringRedisTemplate = new StringRedisTemplate();
        stringRedisTemplate.setConnectionFactory(factory);
        return stringRedisTemplate;
    }

    @Bean
    RedisMessageListenerContainer container(RedisConnectionFactory connectionFactory, MessageListenerAdapter listenerAdapterBack) {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.addMessageListener(listenerAdapterBack, new PatternTopic("groupMessage.*"));
        return container;
    }

}
