package com.refix.refix.config;

import com.refix.refix.filter.KaptchaFilter;
import com.refix.refix.handler.LoginFailureHandler;
import com.refix.refix.service.Impl.UserServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import javax.annotation.Resource;
import javax.sql.DataSource;

/**
 * @Author ODH
 * @Date 2021/5/18 下午 01:38
 * @description
 * @Version 1.0
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
  @Resource
  private DataSource dataSource;
  @Resource
  private LoginFailureHandler loginFailureHandler;
  @Resource
  private UserDetailsService userDetailsService;
  @Resource
  private UserServiceImpl userServiceImpl;

  /**
   * 指定认证对象的来源
   */
  @Override
  public void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(userServiceImpl).passwordEncoder(passwordEncoder());
  }

  /**
   * 加密
   *
   * @return BCryptPasswordEncoder
   */
  @Bean
  public BCryptPasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  /**
   * SpringSecurity配置信息
   */
  @Override
  public void configure(HttpSecurity http) throws Exception {
    http.authorizeRequests()
        .antMatchers("/user/toLogin", "/css/**", "/img/**", "/plugins/**", "/js/**", "/fonts/**", "/sys/kaptcha", "/favicon.ico").permitAll()
        .antMatchers("/**").access("isAuthenticated()")
        .anyRequest().authenticated()
        .and()
        .formLogin()
        .loginPage("/user/toLogin")
        .loginProcessingUrl("/login")
        .successForwardUrl("/user/toIndex")
        .failureHandler(loginFailureHandler)
        .and()
        .logout()
        .logoutUrl("/logout")
        .logoutSuccessUrl("/user/toLogin")
        .invalidateHttpSession(true)
        .deleteCookies("JSESSIONID")
        .and()
        .csrf()
        .disable()
        .headers().frameOptions().sameOrigin();
    //在认证用户名之前认证验证码，如果验证码错误，将不执行用户名和密码的认证
    http.addFilterBefore(new KaptchaFilter("/login", loginFailureHandler), UsernamePasswordAuthenticationFilter.class)
        // 记住我配置
        .rememberMe()
        .rememberMeParameter("remember")
        .rememberMeCookieName("renting")
        // 配置数据库源
        .tokenRepository(persistentTokenRepository())
        .tokenValiditySeconds(3600)
        .userDetailsService(userDetailsService);

  }

  @Bean
  public PersistentTokenRepository persistentTokenRepository() {
    JdbcTokenRepositoryImpl persistentTokenRepository = new JdbcTokenRepositoryImpl();
    // 将 DataSource 设置到 PersistentTokenRepository
    persistentTokenRepository.setDataSource(dataSource);
    persistentTokenRepository.setCreateTableOnStartup(false);
    return persistentTokenRepository;
  }
}
