package com.refix.refix.enums;

/**
 * @Author ODH
 * @Date 2021/5/18 下午 01:45
 * @description
 * @Version 1.0
 */
public enum JsonReturnCode {
    //未登录
    NOT_LOGIN("401", "未登录"),
    //成功
    SUCCESS("200", "成功"),
    //内部失败
    FAIL("500", "内部失败"),
    //禁止访问
    ACCESS_ERROR("403", "禁止访问"),
    //页面未发现
    NOT_FOUND("404", "页面未发现"),
    //密码相同
    SAME("2000", "密码相同"),
    //无需设置密码
    SUCCESSMSG("2001", "无需设置密码"),
    //session过期
    FAIL_SESSION("5001", "session过期"),
    //特殊情况
    FAIL_EXCEPTION("5002", "特殊情况");
    private String code;
    private String desc;

    JsonReturnCode(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}

