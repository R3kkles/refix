package com.refix.refix.enums;

import java.time.Duration;

/**
 * @Author ODH
 * @Date 2021/5/18 下午 01:37
 * @description
 * @Version 1.0
 */
public enum ExpireTimeType {
    GENERAL("GD", "general", Duration.ofHours(1L)),
    HALF_MINUTE("HM", "half-minute", Duration.ofSeconds(30L)),
    ONE_MINUTE("OM", "one minute", Duration.ofMinutes(1L)),
    FIVE_MINUTE("FM", "five minute", Duration.ofMinutes(5L)),
    TEN_MINUTE("TM", "ten minute", Duration.ofMinutes(10L)),
    HALF_HOUR("HH", "half-hour", Duration.ofMinutes(30L)),
    ONE_HOUR("OH", "one hour", Duration.ofHours(1L)),
    TWO_HOUR("TH", "two hour", Duration.ofHours(2L)),
    SIX_HOUR("SH", "six hour", Duration.ofHours(6L)),
    TWELVE_HOUR("TWH", "twelve hour", Duration.ofHours(12L)),
    ONE_DAY("OD", "one day", Duration.ofDays(1L));

    private final String typeName;
    private final String describe;
    private final Duration duration;

    ExpireTimeType(String typeName, String describe, Duration duration) {
        this.typeName = typeName;
        this.describe = describe;
        this.duration = duration;
    }

    public Duration toDuration() {
        return this.duration;
    }

    @Override
    public String toString() {
        return this.typeName;
    }

    public String getDescribe() {
        return this.describe;
    }
}
