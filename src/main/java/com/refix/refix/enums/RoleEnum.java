package com.refix.refix.enums;

/**
 * @Author ODH
 * @Date 2021/5/18 下午 01:44
 * @description
 * @Version 1.0
 */
public enum RoleEnum {
    //管理员，拥有最高权限
    ROLE_Admin("1", "ROLE_Admin"),
    //用户
    ROLE_USER("4", "ROLE_User");

    private String rId;
    private String rName;

    RoleEnum(String rId, String rName) {
        this.rId = rId;
        this.rName = rName;
    }

    public static String getName(String rid) {
        for (RoleEnum temp : values()) {
            if (temp.getrId().equals(rid)) {
                return temp.getrName();
            }
        }
        return rid;
    }

    public String getrId() {
        return rId;
    }

    public void setrId(String rId) {
        this.rId = rId;
    }

    public String getrName() {
        return rName;
    }

    public void setrName(String rName) {
        this.rName = rName;
    }
}

