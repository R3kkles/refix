package com.refix.refix.filter;

import com.google.code.kaptcha.Constants;
import com.refix.refix.handler.LoginFailureHandler;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author ODH
 * @Date 2021/5/18 下午 01:45
 * @description
 * @Version 1.0
 */
public class KaptchaFilter extends AbstractAuthenticationProcessingFilter implements Filter {
  /**
   * SESSION 关于 验证码
   */
  private static final String VRIFY_CODE = "vrifyCode";
  private static final String URL_TYPE = "POST";
  /**
   * 拦截请求地址
   */
  private final String servletPath;

  public KaptchaFilter(String servletPath, LoginFailureHandler loginFailureHandler) {
    super(servletPath);
    this.servletPath = servletPath;
    setAuthenticationFailureHandler(loginFailureHandler);
  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {
    HttpServletRequest req = (HttpServletRequest) request;
    HttpServletResponse res = (HttpServletResponse) response;
    //方便开发，暂时注销
    if (URL_TYPE.equalsIgnoreCase(req.getMethod()) && servletPath.equals(req.getServletPath())) {
      String expect = (String) req.getSession().getAttribute(Constants.KAPTCHA_SESSION_KEY);
      if (expect != null && !expect.equalsIgnoreCase(req.getParameter(VRIFY_CODE))) {
        unsuccessfulAuthentication(req, res, new InsufficientAuthenticationException("codeWrong"));
        return;
      }
    }
    chain.doFilter(req, res);
  }

  @Override
  public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
      throws AuthenticationException {
    return null;
  }

}

