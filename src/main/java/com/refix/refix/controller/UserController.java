package com.refix.refix.controller;

import com.refix.refix.entity.JsonResult;
import com.refix.refix.entity.SysUser;
import com.refix.refix.enums.JsonReturnCode;
import com.refix.refix.service.Impl.SysServiceImpl;
import com.refix.refix.service.UserService;
import com.refix.refix.util.SessionUtil;
import com.refix.refix.util.TreeUtil;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/user")
@Controller
public class UserController {
  private static final String TO_INDEX = "/index.html";
  private static final String TO_LOGIN = "/login.html";
  @Resource
  private BCryptPasswordEncoder bCryptPasswordEncoder;
  @Resource(name = "redisTemplate")
  private RedisTemplate<Object, Object> redisTemplate;
  @Resource
  private SysServiceImpl sysService;
  @Resource
  private UserService userService;

  @RequestMapping("/toIndex")
  public ModelAndView toIndex(HttpServletRequest request, HttpServletResponse response) {
    ModelAndView modelAndView = new ModelAndView();
    SysUser sysUser = (SysUser) SessionUtil.getSession().getAttribute("user");
    String role = sysUser.getRoles().get(0).getRoleCode();
    modelAndView.setViewName(TO_INDEX);
    //渲染菜单
    List<Object> menuTree = TreeUtil.buildTree(sysService.getTree("Menu"), 0);
    //获取主页数据
    modelAndView.addObject("menuList", menuTree);
    return modelAndView;
  }

  @RequestMapping("/toLogin")
  public ModelAndView toLogin(HttpServletRequest request) {
    ModelAndView modelAndView = new ModelAndView();
    Map<String, String> map = new HashMap<>(2);
    String code = (String) request.getAttribute("code");
    if (JsonReturnCode.FAIL.getCode().equals(code)) {
      String errorMsg = (String) request.getAttribute("errorMsg");
      map.put("code", code);
      map.put("errorMsg", errorMsg);
      modelAndView.addObject("result", map);
    }
    modelAndView.setViewName(TO_LOGIN);
    return modelAndView;
  }

  @RequestMapping("/toUpdatePassword")
  public ModelAndView toUpdatePassword() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("common/updatePassword");
    return modelAndView;
  }

  @RequestMapping("/updatePassword")
  @ResponseBody
  public JsonResult<String> updatePassword(String oldPassword, String newPassword, String newPassword2) {
    if (!newPassword.equals(newPassword2)) {
      return JsonResult.failMessage("两次新密码不匹配");
    }
    SysUser sysUser = (SysUser) SessionUtil.getSession().getAttribute("user");
    boolean flag = bCryptPasswordEncoder.matches(oldPassword, sysUser.getPassword());
    int i;
    if (flag) {
      sysUser.setPassword(newPassword);
      i = sysService.updateUser(sysUser);
    } else {
      return JsonResult.failMessage("原密码不匹配");
    }
    if (i > 0) {
      return JsonResult.successMessage("修改密码成功");
    }
    return JsonResult.failMessage("修改密码失败");
  }
}
