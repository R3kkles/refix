package com.refix.refix.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.code.kaptcha.Constants;
import com.google.code.kaptcha.Producer;
import com.refix.refix.entity.*;
import com.refix.refix.service.SysService;
import com.refix.refix.util.FileUtil;
import com.refix.refix.util.RedisUtil;
import com.refix.refix.util.TreeUtil;
import com.refix.refix.util.UUIDUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@RequestMapping("/sys")
@Controller
public class SysController {
  @Resource
  private final Producer captchaProducer = null;
  @Resource(name = "redisTemplate")
  private RedisTemplate<Object, Object> redisTemplate;
  @Resource
  private SysService sysService;
  @Resource
  private RedisUtil redisUtil;

  /**
   * 验证码
   *
   * @param request  HttpServletRequest
   * @param response HttpServletResponse
   * @throws Exception Exception
   */
  @RequestMapping("/kaptcha")
  public void getKaptchaImage(HttpServletRequest request, HttpServletResponse response) throws Exception {
    HttpSession session = request.getSession();
    response.setDateHeader("Expires", 0);
    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
    response.setHeader("Pragma", "no-cache");
    response.setContentType("image/jpeg");
    //生成验证码
    String capText = captchaProducer.createText();
    session.setAttribute(Constants.KAPTCHA_SESSION_KEY, capText);
    //向客户端写出
    BufferedImage bi = captchaProducer.createImage(capText);
    ServletOutputStream out = response.getOutputStream();
    ImageIO.write(bi, "jpg", out);
    try {
      out.flush();
    } finally {
      out.close();
    }
  }

  /**
   * 打开新窗口
   */
  @RequestMapping("/freshenTag")
  public String freshenTag(@RequestBody SysMenu sysMenu, Model model) {
    model.addAttribute("tag", sysMenu);
    return "common/tag::tag";
  }

  /**
   * 获取树
   *
   * @param name
   * @param link
   * @param checkbox
   * @param selects
   * @return
   */
  @RequestMapping("/getTree")
  public ModelAndView getTree(String name, boolean link, boolean checkbox, boolean selects, String linkAction) {
    ModelAndView modelAndView = new ModelAndView();
    List<Object> list = redisUtil.getTree(name);
    modelAndView.addObject("link", link);
    modelAndView.addObject("checkbox", checkbox);
    modelAndView.addObject("selects", selects);
    modelAndView.addObject("treeList", list);
    modelAndView.addObject("linkAction", StringUtils.defaultIfBlank(linkAction, ""));
    modelAndView.setViewName("common/tree::treeModal");
    return modelAndView;
  }

  /**
   * 获取图标库
   *
   * @return
   */
  @RequestMapping("/getIcon")
  public ModelAndView getIcon() {
    ModelAndView modelAndView = new ModelAndView();
    //图标加载
    String path = Objects.requireNonNull(SysController.class.getClassLoader().getResource("bootstrap-icons.json")).getPath();
    String temp = FileUtil.getJson(path);
    JSONObject iconList = JSON.parseObject(temp);
    modelAndView.addObject("iconList", iconList);
    modelAndView.setViewName("common/iconLib::iconLib");
    return modelAndView;
  }

  /**
   * 打开工作台
   *
   * @return
   */
  @RequestMapping("/workbench")
  public ModelAndView goToWorkbench() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("/workbench");
    return modelAndView;
  }

  /**
   * 打开提示框
   *
   * @param str
   * @param html
   * @return
   */
  @RequestMapping("/tip")
  public ModelAndView tip(String str, String html) {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.addObject("str", str);
    modelAndView.setViewName("common/assembly::" + html);
    return modelAndView;
  }


  /**
   * 打开新窗口
   *
   * @param url
   * @param primary_key
   * @return
   */
  @RequestMapping("/openNewWindow")
  public ModelAndView openNewWindow(String url, @RequestParam(required = false) String key) {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.addObject("id", UUIDUtil.uuid());
    modelAndView.addObject("url", url);
    modelAndView.addObject("key", key);
    modelAndView.setViewName("common/assembly::window");
    return modelAndView;
  }

  /**
   * 跳转菜单管理
   *
   * @return
   */
  @RequestMapping("/toMenuMan")
  @Secured({"ROLE_Admin"})
  public ModelAndView toMenuMan() {
    ModelAndView modelAndView = new ModelAndView();
    SysMenu sysMenu = new SysMenu();
    sysMenu.setRid(sysService.queryAllRoles(null));
    modelAndView.addObject("menu", sysMenu);
    modelAndView.addObject("treeList", TreeUtil.buildTree(sysService.getTree("Menu"), 0));
    modelAndView.setViewName("menuMan/menuMan");
    return modelAndView;
  }

  /**
   * 跳转账号管理
   *
   * @param param
   * @return
   */
  @RequestMapping("/toUserMan")
  @Secured({"ROLE_Admin"})
  public ModelAndView toUserMan(@RequestBody(required = false) Map<String, Object> param) {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("userMan/userMan");
    Integer num = 0;
    if (param != null) {
      num = (Integer) param.get("num");
      if (Boolean.parseBoolean((String) param.get("search"))) {
        modelAndView.setViewName("userMan/userMan::userForm");
      }
    }
    PageHelper.startPage(num, 12);
    List<SysUser> list = sysService.queryAllUser(param);
    PageInfo<SysUser> PageInfo = new PageInfo<>(list);
    modelAndView.addObject("userList", PageInfo);
    return modelAndView;
  }

  @RequestMapping("/toModifyUser")
  @Secured({"ROLE_Admin"})
  public ModelAndView toModifyUser(@RequestParam Integer key) {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("userMan/userModify");
    modelAndView.addObject("user", sysService.queryOneUser(key));
    return modelAndView;
  }

  @RequestMapping("/toAddUser")
  @Secured({"ROLE_Admin"})
  public ModelAndView toAddUser() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("userMan/userAdd");
    return modelAndView;
  }

  @RequestMapping("/toRoleMan")
  @Secured({"ROLE_Admin"})
  public ModelAndView toRoleMan(@RequestBody(required = false) Map<String, Object> param) {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("roleMan/roleMan");
    Integer num = 0;
    if (param != null) {
      num = (Integer) param.get("num");
      if (Boolean.parseBoolean((String) param.get("search"))) {
        modelAndView.setViewName("roleMan/roleMan::roleForm");
      }
    }
    PageHelper.startPage(num, 12);
    List<SysRole> list = sysService.queryAllRoles(param);
    PageInfo<SysRole> PageInfo = new PageInfo<>(list);
    modelAndView.addObject("roleList", PageInfo);
    return modelAndView;
  }

  @RequestMapping("/toModifyRole")
  @Secured({"ROLE_Admin"})
  public ModelAndView toModifyRole(@RequestParam Integer key) {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("roleMan/roleModify");
    modelAndView.addObject("role", sysService.queryOneRole(key));
    return modelAndView;
  }

  @RequestMapping("/toAddRole")
  @Secured({"ROLE_Admin"})
  public ModelAndView toAddRole() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("roleMan/roleAdd");
    return modelAndView;
  }

  /**
   * 跳转地区管理
   *
   * @return
   */
  @RequestMapping("/toAreaMan")
  @Secured({"ROLE_Admin"})
  public ModelAndView toAreaMan() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.addObject("area", new SysArea());
    modelAndView.addObject("treeList", redisUtil.getTree("Area"));
    modelAndView.setViewName("areaMan/areaMan");
    return modelAndView;
  }

  /**
   * 跳转组织管理
   *
   * @return
   */
  @RequestMapping("/toOrgMan")
  @Secured({"ROLE_Admin"})
  public ModelAndView toOrgMan() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.addObject("org", new SysOrg());
    modelAndView.addObject("treeList", redisUtil.getTree("Org"));
    modelAndView.setViewName("orgMan/orgMan");
    return modelAndView;
  }

  /**
   * 跳转字典管理
   *
   * @param param
   * @return
   */
  @RequestMapping("/toDictMan")
  @Secured({"ROLE_Admin"})
  public ModelAndView toDictMan(@RequestBody(required = false) Map<String, Object> param) {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("dictMan/dictMan");
    Integer num = 0;
    if (param != null) {
      num = (Integer) param.get("num");
      if (Boolean.parseBoolean((String) param.get("search"))) {
        modelAndView.setViewName("dictMan/dictMan::dictForm");
      }
    }
    PageHelper.startPage(num, 12);
    List<SysDict> list = sysService.queryAllDict(param);
    PageInfo<SysDict> PageInfo = new PageInfo<>(list);
    modelAndView.addObject("dictList", PageInfo);
    return modelAndView;
  }

  @RequestMapping("/toModifyDict")
  @Secured({"ROLE_Admin"})
  public ModelAndView toModifyDict(@RequestParam Integer key) {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("dictMan/dictModify");
    modelAndView.addObject("dict", sysService.queryOneDict(key));
    return modelAndView;
  }

  @RequestMapping("/toAddDict")
  @Secured({"ROLE_Admin"})
  public ModelAndView toAddDict() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("dictMan/dictAdd");
    return modelAndView;
  }


  /**
   * 删除菜单
   *
   * @param id
   * @return
   */
  @RequestMapping("/deleteMenu")
  @Secured({"ROLE_Admin"})
  @ResponseBody
  public JsonResult<SysMenu> deleteMenu(@RequestParam("treeCheckbox") Integer[] id) {
    try {
      if (sysService.deleteMenu(id) > 0) {
        return JsonResult.successMessage("删除成功");
      } else {
        return JsonResult.failMessage("删除失败，请稍后重试");
      }
    } catch (Exception e) {
      return JsonResult.failMessage("删除失败，请稍后重试");
    }
  }

  /**
   * 查找单个菜单
   *
   * @param id
   * @return
   */
  @RequestMapping("/selectOneMenu")
  @Secured({"ROLE_Admin"})
  public ModelAndView queryOneMenu(String id) {
    ModelAndView modelAndView = new ModelAndView();
    SysMenu sysMenu = sysService.queryOneMenu(id);
    sysMenu.setRid(sysService.queryCheckRole(Integer.valueOf(id)));
    modelAndView.addObject("menu", sysMenu);
    modelAndView.setViewName("menuMan/menuMan::menuManDetails");
    return modelAndView;
  }

  /**
   * 保存菜单
   *
   * @param sysMenu
   * @return
   */
  @RequestMapping("/updateMenu")
  @Secured({"ROLE_Admin"})
  @ResponseBody
  public JsonResult<SysMenu> updateMenu(SysMenu sysMenu) {
    try {
      if (sysService.updateMenu(sysMenu) > 0) {
        return JsonResult.successMessage("保存菜单成功");
      } else {
        return JsonResult.failMessage("保存菜单失败，请稍后重试");
      }
    } catch (Exception e) {
      return JsonResult.failMessage("保存菜单失败，请稍后重试");
    }
  }

  @RequestMapping("/getDictSelect")
  public ModelAndView getDictSelect(String id) {
    ModelAndView modelAndView = new ModelAndView();
    List<String> list = new ArrayList<>();
    list.add(id);
    modelAndView.addObject("dictList", sysService.queryDict(list));
    modelAndView.setViewName("common/assembly::option");
    return modelAndView;
  }

  @RequestMapping("/updateUser")
  @ResponseBody
  public JsonResult<String> updateUser(SysUser sysUser) {
    try {
      if (sysService.updateUser(sysUser) > 0) {
        return JsonResult.successMessage("保存账号成功");
      } else {
        return JsonResult.failMessage("保存账号失败，请稍后重试");
      }
    } catch (Exception e) {
      return JsonResult.failMessage("保存账号失败，请稍后重试");
    }
  }

  @RequestMapping("/insertUser")
  @Secured({"ROLE_Admin"})
  @ResponseBody
  public JsonResult<String> insertUser(SysUser sysUser) {
    try {
      if (sysService.insertUser(sysUser) > 0) {
        return JsonResult.successMessage("新增账号成功");
      } else {
        return JsonResult.failMessage("新增菜单失败，请稍后重试");
      }
    } catch (Exception e) {
      return JsonResult.failMessage("新增菜单失败，请稍后重试");
    }
  }

  @RequestMapping("/deleteUser")
  @Secured({"ROLE_Admin"})
  @ResponseBody
  public JsonResult<String> deleteUser(@RequestParam("userManCheckbox") Integer[] id) {
    try {
      if (sysService.deleteUser(id) > 0) {
        return JsonResult.successMessage("删除成功");
      } else {
        return JsonResult.failMessage("删除失败，请稍后重试");
      }
    } catch (Exception e) {
      return JsonResult.failMessage("删除失败，请稍后重试");
    }
  }

  @RequestMapping("/updateRole")
  @ResponseBody
  public JsonResult<String> updateRole(SysRole sysRole) {
    try {
      if (sysService.updateRole(sysRole) > 0) {
        return JsonResult.successMessage("保存权限角色成功");
      } else {
        return JsonResult.failMessage("保存权限角色失败，请稍后重试");
      }
    } catch (Exception e) {
      return JsonResult.failMessage("保存权限角色失败，请稍后重试");
    }
  }

  @RequestMapping("/insertRole")
  @Secured({"ROLE_Admin"})
  @ResponseBody
  public JsonResult<String> insertRole(SysRole sysRole) {
    try {
      if (sysService.insertRole(sysRole) > 0) {
        return JsonResult.successMessage("新增权限角色成功");
      } else {
        return JsonResult.failMessage("新增权限角色失败，请稍后重试");
      }
    } catch (Exception e) {
      return JsonResult.failMessage("新增权限角色失败，请稍后重试");
    }
  }

  @RequestMapping("/deleteRole")
  @Secured({"ROLE_Admin"})
  @ResponseBody
  public JsonResult<String> deleteRole(@RequestParam("roleManCheckbox") Integer[] id) {
    try {
      if (sysService.deleteRole(id) > 0) {
        return JsonResult.successMessage("删除权限角色成功！关联此权限角色的账号将无映射关系，请注意修改无映射关系的账号。");
      } else {
        return JsonResult.failMessage("删除权限角色失败，请稍后重试");
      }
    } catch (Exception e) {
      return JsonResult.failMessage("删除权限角色失败，请稍后重试");
    }
  }


  /**
   * 查找单个地区
   *
   * @param id
   * @return
   */
  @RequestMapping("/selectOneArea")
  @Secured({"ROLE_Admin"})
  public ModelAndView queryOneArea(String id) {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.addObject("area", sysService.queryOneArea(id));
    modelAndView.setViewName("areaMan/areaMan::areaManDetails");
    return modelAndView;
  }

  /**
   * 保存地区
   *
   * @param sysArea
   * @return
   */
  @RequestMapping("/updateArea")
  @Secured({"ROLE_Admin"})
  @ResponseBody
  public JsonResult<String> updateMenu(SysArea sysArea) {
    try {
      if (sysService.updateArea(sysArea) > 0) {
        redisUtil.deleteTree("Area");
        return JsonResult.successMessage("保存地区成功");
      } else {
        return JsonResult.failMessage("保存地区失败，请稍后重试");
      }
    } catch (Exception e) {
      return JsonResult.failMessage("保存地区失败，请稍后重试");
    }
  }

  /**
   * 删除地区
   *
   * @param id
   * @return
   */
  @RequestMapping("/deleteArea")
  @Secured({"ROLE_Admin"})
  @ResponseBody
  public JsonResult<String> deleteArea(@RequestParam("treeCheckbox") Integer[] id) {
    try {
      if (sysService.deleteArea(id) > 0) {
        redisUtil.deleteTree("Area");
        return JsonResult.successMessage("删除成功！关联该地区的账号、组织等地区信息将失效，请及时修改。");
      } else {
        return JsonResult.failMessage("删除失败，请稍后重试");
      }
    } catch (Exception e) {
      return JsonResult.failMessage("删除失败，请稍后重试");
    }
  }

  /**
   * 查找单个组织
   *
   * @param id
   * @return
   */
  @RequestMapping("/selectOneOrg")
  @Secured({"ROLE_Admin"})
  public ModelAndView queryOneOrg(String id) {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.addObject("org", sysService.queryOneOrg(id));
    modelAndView.setViewName("orgMan/orgMan::orgManDetails");
    return modelAndView;
  }

  /**
   * 保存组织
   *
   * @param sysOrg
   * @return
   */
  @RequestMapping("/updateOrg")
  @Secured({"ROLE_Admin"})
  @ResponseBody
  public JsonResult<String> updateMenu(SysOrg sysOrg) {
    try {
      if (sysService.updateOrg(sysOrg) > 0) {
        redisUtil.deleteTree("Org");
        return JsonResult.successMessage("保存组织成功");
      } else {
        return JsonResult.failMessage("保存组织失败，请稍后重试");
      }
    } catch (Exception e) {
      return JsonResult.failMessage("保存组织失败，请稍后重试");
    }
  }

  /**
   * 删除组织
   *
   * @param id
   * @return
   */
  @RequestMapping("/deleteOrg")
  @Secured({"ROLE_Admin"})
  @ResponseBody
  public JsonResult<String> deleteOrg(@RequestParam("treeCheckbox") Integer[] id) {
    try {
      if (sysService.deleteOrg(id) > 0) {
        redisUtil.deleteTree("Org");
        return JsonResult.successMessage("删除成功！关联该组织的账号等组织信息将失效，请及时修改。");
      } else {
        return JsonResult.failMessage("删除失败，请稍后重试");
      }
    } catch (Exception e) {
      return JsonResult.failMessage("删除失败，请稍后重试");
    }
  }

  /**
   * 更新字典
   *
   * @return
   */
  @RequestMapping("/updateDict")
  @Secured({"ROLE_Admin"})
  @ResponseBody
  public JsonResult<String> updateDict(SysDict sysDict) {
    try {
      if (sysService.updateDict(sysDict) > 0) {
        return JsonResult.successMessage("保存字典成功！与该字典关联数据可能失效，请注意！");
      } else {
        return JsonResult.failMessage("保存字典失败，请稍后重试");
      }
    } catch (Exception e) {
      return JsonResult.failMessage("保存字典失败，请稍后重试");
    }
  }

  @RequestMapping("/deleteDict")
  @Secured({"ROLE_Admin"})
  @ResponseBody
  public JsonResult<String> deleteDict(@RequestParam("dictManCheckbox") Integer[] id) {
    try {
      if (sysService.deleteDict(id) > 0) {
        return JsonResult.successMessage("删除成功");
      } else {
        return JsonResult.failMessage("删除失败，请稍后重试");
      }
    } catch (Exception e) {
      return JsonResult.failMessage("删除失败，请稍后重试");
    }
  }


}