package com.refix.refix.Interface;

import java.lang.annotation.*;

/**
 * @Author ODH
 * @Date 2021/5/18 下午 01:46
 * @description 实体类value值
 * @Version 1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
@Documented
public @interface DataName {
    /**
     * 字段名称
     *
     * @return
     */
    String name() default "";
}
