package com.refix.refix.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * @Author ODH
 * @Date 2021/7/9 下午 01:54
 * @Describe
 * @Version 1.0
 */
public class ReflectTool {
  public static Method getSetter(String fieldName, Class<?> cls) {
    for (Method method : cls.getMethods()) {
      if (method.getName().equalsIgnoreCase("set".concat(fieldName))) {
        return method;
      }
    }
    return null;
  }

  /**
   * 通过属性名获取Field对象
   *
   * @param fieldName
   * @param cls
   * @return
   */
  public static synchronized Field getFieldByName(String fieldName, Class<?> cls) {
    Field[] fields = cls.getDeclaredFields();
    for (Field field : fields) {
      if (field.getName().equals(fieldName)) {
        return field;
      }
    }
    if (cls.getSuperclass() != null) {
      return getFieldByName(fieldName, cls.getSuperclass());
    }
    return null;
  }

  /**
   * 获取get方法
   *
   * @param fieldName
   * @param cls
   * @return
   */
  public static Method getGetter(String fieldName, Class<?> cls) {
    for (Method method : cls.getMethods()) {
      if (method.getName().equalsIgnoreCase("get".concat(fieldName)) && method.getParameterTypes().length == 0) {
        return method;
      }
    }
    return null;
  }


  public static Object getValueByGetter(String fieldName, Object obj) throws ReflectiveOperationException {
    Method getter = getGetter(fieldName, obj.getClass());
    if (getter != null) {
      return getter.invoke(obj);
    }
    return null;
  }


  /**
   * 获取字段对应值，并转为String类型，空值返回空字符串
   *
   * @param fieldName
   * @param obj
   * @return
   */
  public static synchronized String getStringValue(String fieldName, Object obj) throws ReflectiveOperationException {
    Object objectValue = getValueByGetter(fieldName, obj);
    if (objectValue == null) {
      return "";
    }
    return objectValue.toString().trim();
  }
}
