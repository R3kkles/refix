package com.refix.refix.util;

import com.refix.refix.service.SysService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author ODH
 * @Date 2021/5/18 下午 01:50
 * @description
 * @Version 1.0
 */
@SuppressWarnings("unchecked")
@Component
public class RedisUtil {

  @Value("${cacheByRedis}")
  private boolean cacheKey;
  @Resource
  private StringRedisTemplate template;
  @Resource
  private RedisTemplate<String, Object> redisTemplate;
  @Resource
  private SysService sysService;

  /**
   * 删除缓存
   *
   * @param name
   */
  public void deleteCache(String name) {
    redisTemplate.delete(name);
  }

  public List<Object> getTree(String name) {
    if (cacheKey && !name.equals("Menu")) {
      if (exists(name)) {
        return (List<Object>) redisTemplate.opsForValue().get(name);
      } else {
        List<Object> list = TreeUtil.buildTree(sysService.getTree(name), 0);
        if (list != null) {
          redisTemplate.opsForValue().set(name, list, -1);
        }
        return list;
      }
    }
    return TreeUtil.buildTree(sysService.getTree(name), 0);
  }

  public void deleteTree(String name) {
    if (cacheKey) {
      if (exists(name)) {
        redisTemplate.delete(name);
      }
    }
  }

  public boolean exists(String key) {
    return Boolean.TRUE.equals(redisTemplate.hasKey(key));
  }

  /**
   * 判断key是否过期
   */
  public boolean isExpire(String key) {
    return expire(key) <= 1;
  }


  /**
   * 从redis中获取key对应的过期时间;
   * 如果该值有过期时间，就返回相应的过期时间;
   * 如果该值没有设置过期时间，就返回-1;
   * 如果没有该值，就返回-2;
   */
  public long expire(String key) {
    return redisTemplate.opsForValue().getOperations().getExpire(key);
  }
}

