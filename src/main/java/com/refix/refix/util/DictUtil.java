package com.refix.refix.util;

import com.refix.refix.dao.SysDao;
import com.refix.refix.entity.SysDict;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Author ODH
 * @Date 2021/7/8 下午 09:38
 * @Describe 字典工具类
 * @Version 1.0
 */
public class DictUtil {
  public static void transformDict(SysDao sysDao, List<?> list, Map<String, String> map) {
    List<String> dictCode = new ArrayList<>();
    List<String> param = new ArrayList<>();
    map.forEach((key, value) -> {
      dictCode.add(key);
      param.add(value);
    });
    List<SysDict> dictList = sysDao.queryDict(dictCode);
    try {
      for (Object o : list) {
        for (String paramTemp : param) {
          String val = ReflectTool.getStringValue(paramTemp, o);
          for (SysDict sysDict : dictList) {
            if (val.equals(sysDict.getValue()) && paramTemp.equals(map.get(sysDict.getCode()))) {
              Method method = ReflectTool.getSetter(paramTemp + "Name", o.getClass());
              assert method != null;
              method.invoke(o, sysDict.getName());
            }
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
