package com.refix.refix.util;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author ODH
 * @Date 2021/5/18 下午 01:51
 * @description
 * @Version 1.0
 */
public class TreeUtil {

  public static List<Object> buildTree(Object obj, Integer pid) {
    try {
      List<Object> list = new ArrayList<>();
      List<Object> res = new ArrayList<>();
      if (obj instanceof ArrayList<?>) {
        list.addAll((List<?>) obj);
      }
      for (Object o : list) {
        if (pid.equals(Integer.parseInt(ReflectTool.getStringValue("parentId", o)))) {
          Method method = ReflectTool.getSetter("childList", o.getClass());
          assert method != null;
          method.invoke(o, buildTree(list, Integer.parseInt(ReflectTool.getStringValue("id", o))));
          res.add(o);
        }
      }
      return res;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }
}

