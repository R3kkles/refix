package com.refix.refix.util;

import java.util.Random;
import java.util.UUID;

/**
 * @Author ODH
 * @Date 2021/5/18 下午 01:51
 * @description
 * @Version 1.0
 */
public class UUIDUtil {
    public static String uuid() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }

    public static String getNumUUID(int strLength) {
        Random rm = new Random();
        double pross = (1 + rm.nextDouble()) * Math.pow(10, strLength);
        String fixLengthString = String.valueOf(pross);
        return fixLengthString.substring(1, strLength + 1);
    }
}

