package com.refix.refix.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Author ODH
 * @Date 2021/5/18 下午 01:49
 * @description
 * @Version 1.0
 */
public class DateUtil {

    public static Date MAX_DATE = maxDate();

    public static String now(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return sdf.format(date);
    }

    public static String now() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(new Date());
    }

    public static String now(String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(new Date());
    }

    private static Date maxDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return sdf.parse("9999-12-31 23:59:59");
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    public static String missTime(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }

    public static Date missDay(String format) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return sdf.parse(format);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
}

