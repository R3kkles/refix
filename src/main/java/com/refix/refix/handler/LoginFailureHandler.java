package com.refix.refix.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author ODH
 * @Date 2021/5/18 下午 01:45
 * @description
 * @Version 1.0
 */
@Component
@Slf4j
public class LoginFailureHandler implements AuthenticationFailureHandler {
    private static final String CODE_MSG = "codeWrong";
    private static final String BAD_CREDENTIALS = "Bad credentials";
    private static final String USERNAME_NOTFOUND = "UserDetailsService returned null, which is an interface contract violation";
    private static final String DISABLED = "User is disabled";
    private static final String LOCKED = "";
    private static final String ACCOUNT_EXPIRED = "";
    private static final String CREDENTIALS_EXPRIED = "";

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException, ServletException {
        String errorMsg = e.getMessage();
        RequestDispatcher dispatcher = request.getRequestDispatcher("/user/toLogin");
        switch (errorMsg) {
            //验证码错误
            case CODE_MSG:
                request.setAttribute("code", "500");
                request.setAttribute("errorMsg", "验证码错误");
                dispatcher.forward(request, response);
                break;
            case BAD_CREDENTIALS:
                request.setAttribute("code", "500");
                request.setAttribute("errorMsg", "密码错误");
                dispatcher.forward(request, response);
                break;
            case USERNAME_NOTFOUND:
                request.setAttribute("code", "500");
                request.setAttribute("errorMsg", "账户不存在或凭证已过期");
                dispatcher.forward(request, response);
                break;
            case DISABLED:
                request.setAttribute("code", "500");
                request.setAttribute("errorMsg", "账户已被禁用，请与管理员联系");
                dispatcher.forward(request, response);
                break;
            default:
                request.setAttribute("code", "500");
                request.setAttribute("errorMsg", "网络错误");
                dispatcher.forward(request, response);
                break;
        }
    }
}

