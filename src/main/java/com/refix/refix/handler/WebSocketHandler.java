package com.refix.refix.handler;

import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * @Author ODH
 * @Date 2021/5/18 下午 01:46
 * @description
 * @Version 1.0
 */
@ServerEndpoint(value = "/newMessageWarning")
@Component
public class WebSocketHandler {
    //用来存放每个客户端对应的MyWebSocket对象。
    private static final CopyOnWriteArraySet<WebSocketHandler> webSocketSet = new CopyOnWriteArraySet<WebSocketHandler>();
    private javax.websocket.Session session = null;

    /**
     * @ClassName: onOpen
     * @Description: 开启连接的操作
     */
    @OnOpen
    public void onOpen(Session session) {
        this.session = session;
        webSocketSet.add(this);
    }

    /**
     * @ClassName: onClose
     * @Description: 连接关闭的操作
     */
    @OnClose
    public void onClose() {
        webSocketSet.remove(this);
    }

    /**
     * @ClassName: onMessage
     * @Description: 给服务器发送消息告知数据库发生变化
     */
    @OnMessage
    public void onMessage(String message) {
        try {
            for (WebSocketHandler item : webSocketSet) {
                item.session.getBasicRemote().sendText(message);
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * @ClassName: OnError
     * @Description: 出错的操作
     */
    @OnError
    public void onError(Throwable error) {
        error.printStackTrace();
    }
}

