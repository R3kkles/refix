package com.refix.refix.dao;

import com.refix.refix.entity.SysRole;
import com.refix.refix.entity.SysUser;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author ODH
 * @Date 2021/2/8 上午 10:24
 * @description
 * @Version 1.0
 */
@Mapper
public interface UserDao {
    List<SysRole> findByUid(Integer uid);

    SysUser findByName(String username);

    int deletePeople(String username);

    int updateUserRoles(SysUser sysUser);

    int register(SysUser sysUser);

    void insertUserRoles(Integer id);
}
