package com.refix.refix.dao;

import com.refix.refix.entity.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @Author ODH
 * @Date 2021/2/15 下午 03:39
 * @description 系统dao
 * @Version 1.0
 */
@Mapper
public interface SysDao {
  /**
   * 新增菜单
   *
   * @param sysMenu
   * @return
   */
  int addModule(SysMenu sysMenu);

  int deleteArea(Integer[] id);

  int deleteOrg(Integer[] id);

  int deleteUser(Integer[] id);

  /**
   * 根据用户id删除映射表
   * @param id
   * @return
   */
  int deleteUserRoleByUID(Integer[] id);

  /**
   * 根据权限id删除映射表
   * @param id
   * @return
   */
  int deleteUserRoleByRID(Integer[] id);

  int insertArea(SysArea sysArea);

  int insertOrg(SysOrg sysOrg);

  int insertUser(SysUser sysUser);

  /**
   * 删除字典
   *
   * @param id
   * @return
   */
  int deleteDict(Integer[] id);

  int deleteMenu(Integer[] id);

  int deleteMenuRole(Integer[] id);

  /**
   * 删除权限
   *
   * @param id
   * @return
   */
  int deleteRole(Integer[] id);

  /**
   * 查询单个字典
   *
   * @param id
   * @return
   */
  SysDict findDictById(String id);

  List<SysMenu> getMenuTree(Integer id);

  /**
   * 新增字典
   *
   * @param sysDict
   * @return
   */
  int insertDict(SysDict sysDict);

  /**
   * 写入日志
   *
   * @param sysLog
   * @return
   */
  int insertLog(SysLog sysLog);

  /**
   * 新增模块
   *
   * @param sysMenu
   * @return
   */
  int insertMenu(SysMenu sysMenu);

  int insertMenuRole(@Param("id") Integer id, @Param("rid") List<SysRole> rid);

  /**
   * 新增权限
   *
   * @param sysRole
   * @return
   */
  int insertRole(SysRole sysRole);

  int insertUserRole(SysUser sysUser);

  /**
   * 所有字典
   *
   * @return
   */
  List<SysDict> queryAllDict(Map<String, Object> param);

  List<SysLog> queryAllLog(Map<String, String> param);

  /**
   * 根据id查询菜单对应权限
   *
   * @param id
   * @return
   */
  Map<String, String> queryAllMenuRoles(String id);

  /**
   * 权限角色树
   *
   * @return
   */
  List<SysRole> queryAllRoles(Map<String, Object> param);

  /**
   * 查询字典
   *
   * @param code
   * @return
   */
  List<SysDict> queryDict(List<String> code);

  SysArea queryOneArea(String id);

  /**
   * 根据id找出一个菜单实体
   *
   * @param id
   * @return
   */
  SysMenu queryOneById(String id);

  SysDict queryOneDict(Integer id);

  SysMenu queryOneMenu(String id);

  /**
   * 查询一个role
   *
   * @param id
   * @return
   */
  List<SysRole> queryCheckRole(Integer id);

  SysOrg queryOneOrg(String id);

  SysRole queryOneRole(Integer id);

  SysUser queryOneUser(Integer id);

  /**
   * 根据id查询用户对应权限
   *
   * @param id
   * @return
   */
  Map<String, Integer> queryUserRoles(String id);

  int updateArea(SysArea sysArea);

  int updateOrg(SysOrg sysOrg);

  int updateUser(SysUser sysUser);

  /**
   * 更新字典
   *
   * @param sysDict
   * @return
   */
  int updateDict(SysDict sysDict);

  /**
   * 更新菜单权限映射表
   *
   * @param sysMenu
   * @return
   */
  int updateMenu(SysMenu sysMenu);

  /**
   * 保存菜单修改
   *
   * @param sysMenu
   * @return
   */
  int updateModule(SysMenu sysMenu);

  /**
   * 更新权限角色
   *
   * @param sysRole
   * @return
   */
  int updateRole(SysRole sysRole);

  /**
   * 查找所有用户（包含搜索）
   *
   * @param param
   * @return
   */
  List<SysUser> queryAllUser(Map<String, Object> param);

  List<SysArea> getAreaTree();

  List<SysOrg> getOrgTree();

}
