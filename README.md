![](https://gitee.com/R3kkles/refix/raw/master/src/main/resources/static/img/ico.gif)

<p align = "center">基于Spring boot、Spring Security、bootstrap的后台管理框架</p>

因为本人能力有限，系统可能出现大大小小bug，如使用中发现，可提出，另外框架并没有适配各种大大小小的屏幕，手机端大小很明确的说并没有进行适配。



# 框架效果图

## 登录

![](https://gitee.com/R3kkles/refix/raw/master/src/main/resources/static/img/image-20210713141253379.png)

## 主页

![](https://gitee.com/R3kkles/refix/raw/master/src/main/resources/static/img/img.png)

## 各项基础设置

![](https://gitee.com/R3kkles/refix/raw/master/src/main/resources/static/img/img_1.png)

![](https://gitee.com/R3kkles/refix/raw/master/src/main/resources/static/img/img_2.png)

# 基本介绍

## 技术选型

​		框架采用spring boot、spring security、thymeleaf、bootstrap、mybatis、mysql、radis，（后续可能会加上中间件监控功能）框架意在提供一个基础平台，供用户根据自身业务或者需求进行二次开发（默认开发者已经了解并熟练运用以上技术，本文不会对非必要语法进行过多解释）。

## 使用

### 菜单管理

​		通过框架，可以快速对框架进行菜单模块动态调整，通过菜单管理可对菜单基础信息进行设置，通过配置点击菜单可决定该菜单是否为点击菜单（点击菜单的含义是当前菜单单机效果是展开菜单还是打开新窗口），同时也可配置菜单可见快速开关某个菜单入口而无需修改代码，当前菜单最大层级为4级，如需更多，可修改菜单碎片（menu.html）以实现符合需求的菜单层数。

### 账号管理

​		通过账号管理，除了基本信息修改，最重要的是为每一个账号赋予权限角色以实现动态菜单与权限控制，一些特定请求地址可使用spring security标签进行权限控制。

​		当前一些关乎框架的基础设置都添加管理员权限，只有管理员能够进行数据访问（如需其他角色，可在代码中修改）。

### 地区、组织、字典管理

​			其中地区与组织管理所进行的操作将在树组件中有所体现，而字典管理将在select标签中有所体检。



## 开发

### 前言

​		个人时间、精力、能力有限，我已经尽可能封装方法，让二次开发尽可能便利，但是可能目前对于市面上一些比较成熟的开发框架（我会去学习一下别的框架是怎么做到的）来说，本框架还是弱一些，日后除了增加更多的基础管理（如中间件监控、在线联系、异常警告、日志生成等），当前你可能会看到一些“建设中的字样”，不需要的请屏蔽，我也会对已有的一些方法进行优化，提供更多的统一构建方法。

### 缓存

​		本框架采用redis作为缓存数据库，默认不开启，如需开启可通过修改YML配置文件中的“cacheByRedis”（注意配置数据库地址），如你的项目没有缓存需求就不必开启了，目前框架设计缓存树组件数据（因为菜单为动态菜单，所以无需缓存），如需修改，可查看RedisUtil工具类

### 树组件

树组件分为两种，一：包含模态框的树组件，通过确定按钮为input赋值，二：无模态框，只能在页面初始化时渲染到页面中。

#### 模态框树组件

​		如果需要完成树组件调用，调用地区树组件实例如下：

```html
        <label class="form-label" for="areaName">地区</label>
        <input class="form-control" th:value="${user.areaName}" id="areaName"
               onclick="getTree(this,'Area',false,true,true,null)" readonly required>
        <input type="hidden" name="areaName" th:value="${user.areaName}">
        <input type="hidden" name="areaId" th:value="${user.areaId}">
```

​		目前调用一个树组件需要三个input，第一个为显示的input，主要用于展示当前选中树数据，而下方两个隐藏input则用于form表单传递数据，点击事件getTree，传入的第二个参数为树组件的名称，当前框架包含树组件有三种：Menu（菜单）、Area（地区）、Org（组织），第三、四、五参数分别为是否需要单击事件、是否需要checkbox选择框、是否需要多选。最后参数为点击事件（点击事件默认传入当前树数据的id），无需点击事件填入null或者""即可。

#### 无模态框树组件

​		参数与模态框书组件并无二致，只不过调用时需要通过服务器返回树信息，通过thymeleaf的引入标签完成渲染，示例如下：

```html
        <div th:replace="/common/tree::tree(${treeList},true,true,false,'manWindow')"></div>
```

#### 自定义树

​		如需实现自定义树结构：

1. 你首先需要编写树实体类，实体类应有如下必须属性：

   ```java
     @DataName(name = "ID")
     private Integer id;
     @DataName(name = "名称")
     private String name;
     @DataName(name = "上级ID，一级树的父ID应为0")
     private Integer parentId = 0;
     @DataName(name = "父名称")
     private String parentName;
     @DataName(name = "编码")
     private String code;
     @DataName(name = "标志，用于判断是否最后子菜单")
     private boolean flag;
     private List<实体类> childList;
   ```

   

2. 在SysService中编写自定义数据库交互方法，方法名应遵循如下规则：get+树名称+Tree，获取到数据库数据后，只需要通过调用TreeUtil.buildTree(Object obj, Integer pid)，传入参数即可，示例如下：

```java
TreeUtil.buildTree(sysService.getTree("Menu"), 0)
```

​		如需将树进行缓存，只需要通过调用RedisUtil.getTree(String 树名称)即可完成缓存，注意树修改时进行缓存更新。

### 标签库

​		添加点击事件getIcon(this)即可调出Bootstrap标签库，示例如下：

```html
<input type="text" id="icon" class="form-control" onclick="getIcon(this)" th:value="${menu.icon}" readonly>
<input type="hidden" name="icon" th:value="${menu.icon}">
```

### 下拉框

​		下拉框涉及字典，在你需要实现下拉框的select标签中添加点击事件getSelect(this,'字典code')"即可

### 打开新窗口

有时候你需要在当前窗口中打开一个新的、覆盖于当前窗口上方的新窗口，你可以通过点击事件

openNewWindow(this,'跳转url','表单id')

其中表单id为打开新窗口前需要检查的表单，后面会有介绍。

### 搜索框/页码

编写属于你的搜索框区域，在每个input中编写search-id="后台接收属性名称，一般与实体类名称相同"，然后提交按钮实现点击按钮pageLoad('表单ID','请求地址',页码)即可，需要刷新的数据区域需要使用form标签覆盖如下：

```html
<form id="表单ID" th:fragment="表单ID">
```

页码只需要编写碎片引入，传入指定参数即可，如下：

```html
<div th:replace="common/assembly::pageInfo('表单ID','请求地址',表单数据)"></div>
```

### 表单

框架默认使用使用“*jquery form*”异步提交。

#### checkbox检查

在需要异步提交的表单中，编写checkbox-check属性，将会对指定form表单进行checkbox选择验证

目前有两种点击方法支持checkbox检查：

1. ```html
   openNewWindow(this,'跳转url','checkbox检查表单ID')
   ```

2. ```html
   submitForm(this,'url','checkbox检查表单ID')
   ```

其中submitForm可以在form标签外提交指定表单到url中，一般可用于批量删除等操作

